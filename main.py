import os
import time
import logging as lo
from collections import namedtuple

import discord
import openai
from dotenv import load_dotenv

load_dotenv()

if os.getenv("ENVIRONMENT") == "testing":
    lo.basicConfig(level=lo.INFO)

openai.api_key = os.getenv("OPENAI_TOKEN")
engine = "ada"
tokens = 5

DISCORD_TOKEN = os.getenv("DISCORD_TOKEN")

bot = discord.Client()

Meme = namedtuple('Meme', ['trigger', 'phrase'])

MEMES = [
    Meme("gema", "dame jemas"),
    Meme("kei", "geisuke bazuquero hp"),
    # Meme("a.", "agano la chupa"),
    Meme("excel", "!excel"),
    Meme("alt", "alt trapo sucio báñate"),
    Meme("guiso", "guiso come pasto amante de las loliconas furras"),
    Meme("viper-bot", "si tan bueno es viper-bot, por que no existe viper-bot-2"),
    Meme("funa", "https://cdn.discordapp.com/attachments/580737950787305486/919727201719562290/Star_Finger_Circle.mp4"),
    Meme("nextrom", "rom masoquista ¡péguenme!"),
    Meme("alekay", "compra desodorante en vez de tanta gema"),
    Meme("dereck", """Padre nuestro que estás en el cielo,
santificado sea tu Nombre;
venga a nosotros tu Reino;
hágase tu voluntad
en la tierra como en el cielo.
Danos hoy
nuestro pan de cada día;
perdona nuestras ofensas,
como también nosotros perdonamos
a los que nos ofenden;
no nos dejes caer en la tentación,
y líbranos del mal.

Amén."""),
    # Meme("sex")
    Meme("linux", "no pronunciarás la L-palabra mientras vigile"),
    Meme("sex", "https://cdn.discordapp.com/attachments/580737950787305486/920046029657759784/essex.mp4"),
    Meme("chile", "https://cdn.discordapp.com/attachments/824400565990719498/916862789333684254/chile.mp4"),

]

@bot.event
async def on_ready():
    lo.info(f">>> {bot.user} connected")
    lo.info(f">>> at {bot.guilds}")

    time.sleep(2)
    await bot.change_presence(status=discord.Status.invisible)


@bot.event
async def on_message(message: discord.Message):
    lo.info(f">>> {message}")
    lo.info(f">>> {message.author}")

    if message.author == bot.user:
        return

    for m in MEMES:
        if m.trigger in message.content.lower():
            lo.info(f"sending {m=}")
            await message.channel.send(m.phrase)


bot.run(DISCORD_TOKEN)
